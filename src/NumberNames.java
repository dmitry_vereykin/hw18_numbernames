/**
 * Created by Dmitry on 7/2/2015.
 */

import java.util.Scanner;

public class NumberNames {

    public static void main(String[] args) {
        Scanner keyboard = new Scanner(System.in);
        String number;
        String numberFinalEng;
        String numberFinalFr;
        String numberFinalDe;
        int input;

        do {
            System.out.print("Please enter a number (0 to 999,999) or -1 to quit: ");
            input = keyboard.nextInt();

            if (input == -1) break;

            if ((input < -1) || (input > 999999)) {
                System.out.println("The number is negative or greater than 999,999, enter another number please: ");
                System.out.println("-----------------------------------------------------------------------------------------");
            } else {
                number = Integer.toString(input);
                char[] numberArray = number.toCharArray();
                numberFinalEng = convertEng(numberArray);
                numberFinalFr = convertFr(numberArray);
                numberFinalDe = convertDe(numberArray);
                System.out.println(" ");
                System.out.println(number + "  in English   =  " + numberFinalEng);
                System.out.println(number + "  in French   =  " + numberFinalFr);
                System.out.println(number + "  in German =  " + numberFinalDe);
                System.out.println("---------------------------------------------------");
            }

            } while (input != -1) ;

            keyboard.close();
        }


    public static String convertEng(char[] numberArray) {
        String finNameNumber = "";
        int length = numberArray.length;

        if (numberArray[0] == '0' && numberArray.length == 1) {return "zero";}

        if (numberArray[0] != '0' && numberArray.length == 1) {
            char oneToNine1 = numberArray[length - 1];
            finNameNumber +=  convertLessTen(oneToNine1);
        }

        if (numberArray.length == 2) {
            char tens1 = numberArray[length - 2];
            char oneToNine2 = numberArray[length - 1];
            finNameNumber +=  convertLessHundred(tens1, oneToNine2);
        }

        if (numberArray.length == 3) {
            char hundreds3 = numberArray[length - 3];
            char tens3 = numberArray[length - 2];
            char oneToNine3 = numberArray[length - 1];

            finNameNumber +=  convertLessTen(hundreds3) + "hundred " + convertLessHundred(tens3, oneToNine3);
        }

        if (numberArray.length == 4) {
            char thousands4 = numberArray[length - 4];
            char hundreds4 = numberArray[length - 3];
            char tens4 = numberArray[length - 2];
            char oneToNine4 = numberArray[length - 1];

            finNameNumber += convertLessTen(thousands4) + "thousand " + checkHundreds(hundreds4) + convertLessHundred(tens4, oneToNine4);
        }

        if (numberArray.length == 5) {
            char thousands5_1 = numberArray[length - 5];
            char thousands5_2 = numberArray[length - 4];
            char hundreds5 = numberArray[length - 3];
            char tens5 = numberArray[length - 2];
            char oneToNine5 = numberArray[length - 1];

            finNameNumber += convertLessHundred(thousands5_1, thousands5_2) + "thousand " + checkHundreds(hundreds5) + convertLessHundred(tens5, oneToNine5);
        }

        if (numberArray.length == 6) {
            char thousands6_1 = numberArray[length - 6];
            char thousands6_2 = numberArray[length - 5];
            char thousands6_3 = numberArray[length - 4];
            char hundreds6 = numberArray[length - 3];
            char tens6 = numberArray[length - 2];
            char oneToNine6 = numberArray[length - 1];

            finNameNumber += convertLessTen(thousands6_1) + "hundred " + convertLessHundred(thousands6_2, thousands6_3) + "thousand " + checkHundreds(hundreds6) + convertLessHundred(tens6, oneToNine6);
        }
        return finNameNumber;
    }

    public static String convertLessTen(char numberChar){
        String name = "";
        switch (numberChar) {
            case '1':  name = "one ";
                break;
            case '2':  name = "two ";
                break;
            case '3':  name = "three ";
                break;
            case '4':  name = "four ";
                break;
            case '5':  name = "five ";
                break;
            case '6':  name = "six ";
                break;
            case '7':  name = "seven ";
                break;
            case '8':  name = "eight ";
                break;
            case '9':  name = "nine ";
                break;
            default: name = "";
        }
        return name;
    }

    public static String convertLessTwenty(char numberChar){
        String name = "";
        switch (numberChar) {
            case '0':  name = "ten ";
                break;
            case '1':  name = "eleven ";
                break;
            case '2':  name = "twelve ";
                break;
            case '3':  name = "thirteen ";
                break;
            case '4':  name = "fourteen ";
                break;
            case '5':  name = "fifteen ";
                break;
            case '6':  name = "sixteen ";
                break;
            case '7':  name = "seventeen ";
                break;
            case '8':  name = "eighteen ";
                break;
            case '9':  name = "nineteen ";
                break;
            default: name = "";
        }
        return name;
    }

    public static String convertLessHundred(char tens, char ones){
        String name = "";
        switch (tens) {
            case '0': name = convertLessTen(ones);
                break;

            case '1':  if (ones == '0')
                name = "ten ";
                else
                name = convertLessTwenty(ones);
                break;

            case '2':  if (ones == '0')
                name = "twenty ";
            else
                name = "twenty-" + convertLessTen(ones);
                break;

            case '3':  if (ones == '0')
                name = "thirty ";
            else
                name = "thirty-" + convertLessTen(ones);
                break;

            case '4':  if (ones == '0')
                name = "forty ";
            else
                name = "forty-" + convertLessTen(ones);
                break;

            case '5':  if (ones == '0')
                name = "fifty ";
            else
                name = "fifty-" + convertLessTen(ones);
                break;

            case '6':  if (ones == '0')
                name = "sixty ";
            else
                name = "sixty-" + convertLessTen(ones);
                break;

            case '7':  if (ones == '0')
                name = "seventy ";
            else
                name = "seventy-" + convertLessTen(ones);
                break;

            case '8':  if (ones == '0')
                name = "eighty ";
            else
                name = "eighty-" + convertLessTen(ones);
                break;

            case '9':  if (ones == '0')
                name = "ninety ";
            else
                name = "ninety-" + convertLessTen(ones);
                break;

            default: name = "";
        }
        return name;
    }

    public static String checkHundreds(char numberChar){
        String name;
        if (numberChar =='0')
            name = "";
        else
            name = convertLessTen(numberChar) + "hundred ";
        return name;
    }


    //French lang

    public static String convertFr(char[] numberArray) {
        String finNameNumber = "";
        int length = numberArray.length;

        if (numberArray[0] == '0' && numberArray.length == 1) {return "zéro";}

        if (numberArray[0] != '0' && numberArray.length == 1) {
            char oneToNine1 = numberArray[length - 1];
            finNameNumber +=  convertLessTenFr(oneToNine1);
        }

        if (numberArray.length == 2) {
            char tens1 = numberArray[length - 2];
            char oneToNine2 = numberArray[length - 1];
            finNameNumber +=  convertLessHundredFr(tens1, oneToNine2);
        }

        if (numberArray.length == 3) {
            char hundreds3 = numberArray[length - 3];
            char tens3 = numberArray[length - 2];
            char oneToNine3 = numberArray[length - 1];

            finNameNumber +=  checkHundredsFr(hundreds3) + convertLessHundredFr(tens3, oneToNine3);
        }

        if (numberArray.length == 4) {
            char thousands4 = numberArray[length - 4];
            char hundreds4 = numberArray[length - 3];
            char tens4 = numberArray[length - 2];
            char oneToNine4 = numberArray[length - 1];

            if (thousands4 == '1')
                    finNameNumber +=  "mille " + checkHundredsFr(hundreds4) + convertLessHundredFr(tens4, oneToNine4);
            else
                    finNameNumber += convertLessTenFr(thousands4) + "mille " + checkHundredsFr(hundreds4) + convertLessHundredFr(tens4, oneToNine4);
        }

        if (numberArray.length == 5) {
            char thousands5_1 = numberArray[length - 5];
            char thousands5_2 = numberArray[length - 4];
            char hundreds5 = numberArray[length - 3];
            char tens5 = numberArray[length - 2];
            char oneToNine5 = numberArray[length - 1];

            finNameNumber += convertLessHundredFr(thousands5_1, thousands5_2) + "mille " + checkHundredsFr(hundreds5) + convertLessHundredFr(tens5, oneToNine5);
        }

        if (numberArray.length == 6) {
            char thousands6_1 = numberArray[length - 6];
            char thousands6_2 = numberArray[length - 5];
            char thousands6_3 = numberArray[length - 4];
            char hundreds6 = numberArray[length - 3];
            char tens6 = numberArray[length - 2];
            char oneToNine6 = numberArray[length - 1];

            finNameNumber += checkHundredsFr(thousands6_1) + convertLessHundredFr(thousands6_2, thousands6_3) + "mille " + checkHundredsFr(hundreds6) + convertLessHundredFr(tens6, oneToNine6);
        }
        return finNameNumber;
    }

    public static String convertLessTenFr(char numberChar){
        String name = "";
        switch (numberChar) {
            case '1':  name = "un ";
                break;
            case '2':  name = "deux ";
                break;
            case '3':  name = "trois ";
                break;
            case '4':  name = "quatre ";
                break;
            case '5':  name = "cinq ";
                break;
            case '6':  name = "six ";
                break;
            case '7':  name = "sept ";
                break;
            case '8':  name = "huit ";
                break;
            case '9':  name = "neuf ";
                break;
            default: name = "";
        }
        return name;
    }

    public static String convertLessTwentyFr(char numberChar){
        String name = "";
        switch (numberChar) {
            case '0':  name = "dix ";
                break;
            case '1':  name = "onze ";
                break;
            case '2':  name = "douze ";
                break;
            case '3':  name = "treize ";
                break;
            case '4':  name = "quatorze ";
                break;
            case '5':  name = "quinze ";
                break;
            case '6':  name = "seize ";
                break;
            case '7':  name = "dix-sept ";
                break;
            case '8':  name = "dix-huit ";
                break;
            case '9':  name = "dix-neuf ";
                break;
            default: name = "";
        }
        return name;
    }

    public static String convertLessHundredFr(char tens, char ones){
        String name = "";
        switch (tens) {
            case '0': name = convertLessTenFr(ones);
                break;

            case '1':  if (ones == '0')
                name = "dix ";
                else
                name = convertLessTwentyFr(ones);
                break;

            case '2':  if (ones == '0')
                name = "vingt ";
             else if (ones == '1')
                name = "vingt et un ";
             else
                name = "vingt" + "-" + convertLessTenFr(ones);
                break;

            case '3':  if (ones == '0')
                name = "trente ";
            else if (ones == '1')
                name = "trente et un";
            else
                name = "trente" + "-" + convertLessTenFr(ones);
                break;

            case '4':  if (ones == '0')
                name = "quarante ";
            else if (ones == '1')
                name = "quarante et un ";
            else
                name = "quarante" + "-" + convertLessTenFr(ones);
                break;

            case '5':  if (ones == '0')
                name = "cinquante ";
            else if (ones == '1')
                name = "cinquante et un ";
            else
                name = "cinquante" + "-" + convertLessTenFr(ones);
                break;

            case '6':  if (ones == '0')
                name = "soixante ";
            else if (ones == '1')
                name = "soixante et un";
            else
                name = "soixante" + "-" + convertLessTenFr(ones);
                break;

            case '7': if (ones == '1')
                name = "soixante-et-onze ";
            else
                name = "soixante" + "-" + convertLessTwentyFr(ones);
                break;

            case '8':  if (ones == '0')
                name = "quatre-vingts ";
            else
                name = "quatre-vingts" + "-" + convertLessTenFr(ones);
                break;

            case '9':  name = "quatre-vingt" + "-" + convertLessTwentyFr(ones);
                break;

            default: name = "";
        }
        return name;
    }

    public static String checkHundredsFr(char numberChar){
        String name;
        if (numberChar =='0')
            name = "";
        else if (numberChar =='1')
            name = "cent ";
        else
            name = convertLessTenFr(numberChar) + "cents ";
        return name;
    }

    //Deutsch lang

    public static String convertDe(char[] numberArray) {
        String finNameNumber = "";
        int length = numberArray.length;

        if (numberArray[0] == '0' && numberArray.length == 1) {return "null";}

        if (numberArray[0] != '0' && numberArray.length == 1) {
            char oneToNine1 = numberArray[length - 1];
            finNameNumber +=  convertLessTenDe(oneToNine1);
        }

        if (numberArray.length == 2) {
            char tens1 = numberArray[length - 2];
            char oneToNine2 = numberArray[length - 1];
            finNameNumber +=  convertLessHundredDe(tens1, oneToNine2);
        }

        if (numberArray.length == 3) {
            char hundreds3 = numberArray[length - 3];
            char tens3 = numberArray[length - 2];
            char oneToNine3 = numberArray[length - 1];

            finNameNumber +=  checkHundredsDe(hundreds3) + convertLessHundredDe(tens3, oneToNine3);
        }

        if (numberArray.length == 4) {
            char thousands4 = numberArray[length - 4];
            char hundreds4 = numberArray[length - 3];
            char tens4 = numberArray[length - 2];
            char oneToNine4 = numberArray[length - 1];

            if (thousands4 == '1')
                finNameNumber +=  "eintausend" + checkHundredsDe(hundreds4) + convertLessHundredDe(tens4, oneToNine4);
            else
                finNameNumber += convertLessTenDe(thousands4) + "tausend" + checkHundredsDe(hundreds4) + convertLessHundredDe(tens4, oneToNine4);

        }

        if (numberArray.length == 5) {
            char thousands5_1 = numberArray[length - 5];
            char thousands5_2 = numberArray[length - 4];
            char hundreds5 = numberArray[length - 3];
            char tens5 = numberArray[length - 2];
            char oneToNine5 = numberArray[length - 1];

            finNameNumber += convertLessHundredDe(thousands5_1, thousands5_2) + "tausend" + checkHundredsDe(hundreds5) + convertLessHundredDe(tens5, oneToNine5);
        }

        if (numberArray.length == 6) {
            char thousands6_1 = numberArray[length - 6];
            char thousands6_2 = numberArray[length - 5];
            char thousands6_3 = numberArray[length - 4];
            char hundreds6 = numberArray[length - 3];
            char tens6 = numberArray[length - 2];
            char oneToNine6 = numberArray[length - 1];

            finNameNumber += checkHundredsDe(thousands6_1) + convertLessHundredDe(thousands6_2, thousands6_3) + "tausend" + checkHundredsDe(hundreds6) + convertLessHundredDe(tens6, oneToNine6);
        }
        return finNameNumber;
    }

    public static String convertLessTenDe(char numberChar){
        String name = "";
        switch (numberChar) {
            case '1':  name = "eins";
                break;
            case '2':  name = "zwei";
                break;
            case '3':  name = "drei";
                break;
            case '4':  name = "vier";
                break;
            case '5':  name = "fünf";
                break;
            case '6':  name = "sechs";
                break;
            case '7':  name = "sieben";
                break;
            case '8':  name = "acht";
                break;
            case '9':  name = "neun";
                break;
            default: name = "";
        }
        return name;
    }

    public static String convertLessTwentyDe(char numberChar){
        String name = "";
        switch (numberChar) {
            case '0':  name = "zehn";
                break;
            case '1':  name = "elf";
                break;
            case '2':  name = "zwölf";
                break;
            case '3':  name = "dreizehn";
                break;
            case '4':  name = "vierzehn";
                break;
            case '5':  name = "fünfzehn";
                break;
            case '6':  name = "sechzehn";
                break;
            case '7':  name = "siebzehn";
                break;
            case '8':  name = "achtzehn";
                break;
            case '9':  name = "neunzehn";
                break;
            default: name = "";
        }
        return name;
    }

    public static String convertLessHundredDe(char tens, char ones){
        String name = "";
        switch (tens) {
            case '0': name = convertLessTenDe(ones);
                break;

            case '1': if (ones == '0')
                name = "zehn";
                else
                name = convertLessTwentyDe(ones);
                break;

            case '2':  if (ones == '0')
                name = "zwanzig";
            else if (ones == '1')
                name = "einundzwanzig";
            else
                name = convertLessTenDe(ones) + "undzwanzig";
                break;

            case '3':  if (ones == '0')
                name = "dreißig";
            else if (ones == '1')
                name = "einunddreißig";
            else
                name = convertLessTenDe(ones) + "unddreißig";
                break;

            case '4':  if (ones == '0')
                name = "vierzig";
            else if (ones == '1')
                name = "einundvierzig";
            else
                name = convertLessTenDe(ones) + "undvierzig";
                break;

            case '5':  if (ones == '0')
                name = "fünfzig";
            else if (ones == '1')
                name = "einundfünfzig";
            else
                name = convertLessTenDe(ones) + "undfünfzig";
                break;

            case '6':  if (ones == '0')
                name = "sechzig";
            else if (ones == '1')
                name = "einundsechzig";
            else
                name = convertLessTenDe(ones) + "undsechzig";
                break;

            case '7':  if (ones == '0')
                name = "siebzig";
            else if (ones == '1')
                name = "einundsiebzig";
            else
                name = convertLessTenDe(ones) + "undsiebzig";
                break;

            case '8':  if (ones == '0')
                name = "achtzig";
            else if (ones == '1')
                name = "einundachtzig";
            else
                name = convertLessTenDe(ones) + "undachtzig";
                break;

            case '9':  if (ones == '0')
                name = "neunzig";
            else if (ones == '1')
                name = "einundneunzig";
            else
                name = convertLessTenDe(ones) + "undneunzig";
                break;

            default: name = "";
        }
        return name;
    }

    public static String checkHundredsDe(char numberChar){
        String name;
        if (numberChar =='0')
            name = "";
        else if (numberChar =='1')
            name = "einhundert";
        else
            name = convertLessTenDe(numberChar) + "hundert";
        return name;
    }
}